var request = require('request');
var moment = require('moment');
var NodeHelper = require("node_helper");

module.exports = NodeHelper.create({
	
	start: function() {
		console.log("Starting node helper: " + this.name);
		
	},
	
	socketNotificationReceived: function(notification, payload) {
		var self = this;
		var logTime = moment();
		console.log("Notification: " + notification + /*" Payload: " + payload +*/ " >> " + logTime.format("YYYY.MM.DD-HH:mm:ss"));
		
		if(notification === "GET_STATION_SCHEDULE"){
			
			var leaderboardJsonUri = payload.config.leaderboardJsonUrl;
			
			request({url: leaderboardJsonUri, qs: {station: payload.config.station}}, function (error, response, body) {
				if (!error && response.statusCode == 200) {
					//console.log(body);
					// get schedule for tomorrow
					var tmrrw = moment().add(1, 'days');
					request({url: leaderboardJsonUri, qs: {station: payload.config.station, date: tmrrw.format("YYYY.MM.DD")}}, function (error2, response2, body2) {
						if (!error2 && response2.statusCode == 200) {
							//console.log(body);
							var pl1 = JSON.parse(body);
							var pl2 = JSON.parse(body2);
							var snd = { station : pl1.station,
										trains1: pl1.trains,
										date1: pl1.date,
										trains2: pl2.trains,
										date2: pl2.date};
							self.sendSocketNotification("STATION_SCHEDULE", snd);
						} else {
							console.log(error2);
							console.log(response2.statusCode);
							self.sendSocketNotification("STATION_SCHEDULE_FAIL", "");
						}
					}).on('error', function (e) {
						// General error
						console.log(e);
						self.sendSocketNotification("STATION_SCHEDULE_FAIL", "");
					});
				} else {
					console.log(error);
					console.log(response.statusCode);
					self.sendSocketNotification("STATION_SCHEDULE_FAIL", "");
				}
			}).on('error', function (e) {
				// General error
				console.log(e);
				self.sendSocketNotification("STATION_SCHEDULE_FAIL", "");
			});
			
		} else if(notification==="LOG_THIS"){
			console.log("USER_PRESENCE: " + payload);
		}
		
	},
});
