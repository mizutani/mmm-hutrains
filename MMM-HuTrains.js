Module.register("MMM-HuTrains",{
	
	// Default module config.
	defaults: {
		leaderboardJsonUrl : "http://apiv2.oroszi.net/elvira/leaderboard",
		station : "Komárom",
		updateInterval : 1000 * 60 * 5, // 5 minutes
		displayCount : 18,
		displaySize: 'normal', // normal or small
		animationSpeed: 2000,
	},
	
	start: function() {
		Log.info("Starting module: " + this.name);

		moment.locale(config.language);

		this.trains = [];
		this.stationName = "";
		this.user_presence = true;
		this.isScheduled = false;
		
		this.loaded = false;
		this.scheduleUpdate(500);
	},
	
	// Define required scripts.
	getScripts: function() {
		return ["moment.js"];
	},
	
	getStationSchedule: function() {
		var logTime = moment();
		Log.info(this.name + ": getting schedule >> " + logTime.format("YYYY.MM.DD-HH:mm:ss"));
		
		this.sendSocketNotification("GET_STATION_SCHEDULE", {config: this.config});	
	},
	
	socketNotificationReceived: function(notification, payload) {
		var logTime = moment();
		Log.info(this.name + ": notification: " + notification + /*" Payload: " + payload +*/ " >> " + logTime.format("YYYY.MM.DD-HH:mm:ss"));
		
		if(notification === "STATION_SCHEDULE"){
			this.isScheduled = false;
			this.loaded = true;
			this.trains1 = payload.trains1;
			this.date1 = payload.date1;
			this.trains2 = payload.trains2;
			this.date2 = payload.date2;
			this.stationName = payload.station;
			this.updateDom(this.config.animationSpeed);
			this.scheduleUpdate();
		} else if(notification === "STATION_SCHEDULE_FAIL"){
			this.isScheduled = false;
			this.sendNotification("SHOW_ALERT", {type: "notification", title:this.name, message:"Request error: Failed to fetch data!"}); 
			this.scheduleUpdate();
		}
	},

	notificationReceived: function(notification, payload) {
		if (notification === "USER_PRESENCE") {
			Log.info(this.name + ": USER_PRESENCE: " + payload);
			this.user_presence = payload;		
			//this.sendSocketNotification("LOG_THIS", payload);
			if (this.user_presence) {
				this.scheduleUpdate(500);
			}
		}
	},
	
	// Override dom generator.
	getDom: function() {
		var wrapper = document.createElement("div");

		var stationWrapper = document.createElement("header");
		if (this.config.displaySize != 'small') {
			stationWrapper.className = "small";
		}
		stationWrapper.innerHTML = this.stationName;

		var tableWrapper = document.createElement("table");

		var tableSize = 'small';
		if (this.config.displaySize === 'small') {
			tableSize = 'xsmall';
		}
		
		tableWrapper.className = "normal " + tableSize + " light";

		if (!this.loaded) {
			var loadingWrapper = document.createElement("tr");
			loadingWrapper.innerHTML = "Loading ...";
			loadingWrapper.className = "dimmed light small";
			tableWrapper.appendChild(loadingWrapper);
			wrapper.appendChild(tableWrapper);
			return wrapper;
		}

		wrapper.appendChild(stationWrapper);
		wrapper.appendChild(tableWrapper);

		var trh = document.createElement("tr");
		tableWrapper.appendChild(trh);

		var td = document.createElement("th");
		td.className = "align-center";
		td.innerHTML = "&nbsp;&nbsp;Érkezés&nbsp;&nbsp;";

		trh.appendChild(td);				

		var td1 = document.createElement("th");
		td1.className = "align-center";
		td1.innerHTML = "&nbsp;&nbsp;Indulás&nbsp;&nbsp;";

		trh.appendChild(td1);		

		var td2 = document.createElement("th");
		td2.className = "align-center";
		td2.innerHTML = "&nbsp;&nbsp;Diff.&nbsp;&nbsp;";

		trh.appendChild(td2);		

		var td3 = document.createElement("th");
		td3.className = "align-center";
		td3.innerHTML = "Vonat";

		trh.appendChild(td3);		

		var i = 0;
		var timeString = moment();

		for (var c in this.trains1) {

			var trainTimeUX = this.getTrainTime(this.trains1[c].real, this.trains1[c].schedule, this.date1);

			if ( trainTimeUX.isAfter(timeString) ) {

				this.writeTrainRow(this.trains1[c], tableWrapper);

				i = i + 1;

				if (i===this.config.displayCount) {
					break;
				}
			}
		}

		if ( i < this.config.displayCount) {
			for (var c in this.trains2) {

				var trainTimeUX = this.getTrainTime(this.trains2[c].real, this.trains2[c].schedule, this.date2);

				if ( trainTimeUX.isAfter(timeString) ) {

					this.writeTrainRow(this.trains2[c], tableWrapper);	

					i = i + 1;

					if (i===this.config.displayCount) {
						break;
					}
				}
			}			
		}
		
		return wrapper;
	},

	writeTrainRow: function(train, tableHTML) {
		var trainType = "";
		if (train.info.type == null) {
			trainType = " bright light";
		}
		if (train.info.type === "passenger") {
			trainType = " dimmed";
		}

		var tr = document.createElement("tr");
		tableHTML.appendChild(tr);

		var td = document.createElement("td");
		td.className = "align-center" + trainType;
		var trainArrival = this.getTrainArrival(train.real, train.schedule);
		td.innerHTML = (trainArrival === train.schedule.arrival) ? train.schedule.arrival : trainArrival + "<br/>(" + train.schedule.arrival + ")" ;

		tr.appendChild(td);

		var td1 = document.createElement("td");
		td1.className = "align-center" + trainType;
		var trainDeparture = this.getTrainDeparture(train.real, train.schedule);
		td1.innerHTML = (trainDeparture === train.schedule.departure) ? train.schedule.departure : trainDeparture + "<br/>(" + train.schedule.departure + ")";

		tr.appendChild(td1);

		var td2 = document.createElement("td");
		td2.className = "align-center" + trainType;
		td2.innerHTML = this.getTrainLate(train.real, train.schedule);

		tr.appendChild(td2);

		var td3 = document.createElement("td");
		td3.className = "align-left" + trainType;
		td3.innerHTML = train.info.text + "&nbsp;&nbsp;&nbsp;" + train.info.info + "<br/>" + train.line;

		tr.appendChild(td3);
	},

	getTrainTime: function(real, schedule, wdate) {
		var trainTimeRaw = this.getTrainArrival(real, schedule);
		if (trainTimeRaw === "") {
			trainTimeRaw = this.getTrainDeparture(real, schedule);
		}
		var trainTime = moment(wdate + " " + trainTimeRaw, "YYYY.MM.DD HH:mm");

		return trainTime;
	},

	getTrainArrival: function(real, schedule) {
		var arrival = (real == null) ? schedule.arrival : real.arrival;

		return arrival;
	},

	getTrainDeparture: function(real, schedule) {
		var departure = (real == null) ? schedule.departure : real.departure;

		return departure;
	},

	getTrainLate: function(real, schedule) {
		var trainTimeLate = "";
		if ((real !== null) && (real.arrival !== "")) {
			var trainTimeReal = moment(real.arrival, "HH:mm");
			var trainTimeExp = moment(schedule.arrival, "HH:mm");
			trainTimeLate = trainTimeReal.diff(trainTimeExp, 'minutes');
			if (trainTimeLate === 0) {
				trainTimeLate = "";
			}
		}
		
		return trainTimeLate;
	},
	
	scheduleUpdate: function(delay) {
		if (!this.isScheduled) {
			var nextLoad = this.config.updateInterval;
			if (typeof delay !== "undefined" && delay >= 0) {
				nextLoad = delay;
			}

			var logTime = moment();
			Log.info(this.name + ": scheduleUpdate " + nextLoad + " >> " + logTime.format("YYYY.MM.DD-HH:mm:ss") + " User: " + this.user_presence);

			if (this.user_presence) {
				this.isScheduled = true;
				var self = this;
				
				setTimeout(function() {
					self.getStationSchedule();
				}, nextLoad);
			}
		}
	}
	
});
