# MMM-HuTrains
Station monitor for the Hungarian State Railways.
This module is an extension of the MagicMirror<sup>2</sup> project.

## Dependencies
  * A [MagicMirror<sup>2</sup>](https://github.com/MichMich/MagicMirror) installation

## Installation
  1. Clone this repo into your `modules` directory.
  2. Create an entry in your `config.js` file to tell this module where to display on screen.
  
 **Example:**
```
 {
    module: 'MMM-HuTrains',
	position: 'top_left',
	config: {
		station: 'Budapest-Déli',
		updateInterval: 1000 * 60 * 3, // 3 minutes
		displayCount: 12,
		displaySize: 'small'	
	}
 },
```

## Config
| **Option** | **Description** |
| --- | --- |
| `station` | Name of the station<br><br>**Example:** `Budapest-Déli`<br>This value is **REQUIRED** |
| `updateInterval` | How often does the content needs to be fetched? (Milliseconds)<br><br>**Default value:** `300000` |
| `displayCount` | How many trains should be displayed?<br><br>**Default value:** `18` |
| `displaySize` | Character size<br><br>**Possible values:** `normal`, `small`<br>**Default value:** `normal` |
